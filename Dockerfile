FROM ubuntu
RUN apt update && apt install -y apache2
COPY html/ /var/www/html/
COPY apache2.conf /etc/apache2/apache2.conf
EXPOSE 80 443
CMD ["apache2ctl", "-D", "FOREGROUND"]
