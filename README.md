**Introducción**

**En el PDF se encuentran adjuntas imágenes que hacen que el TP esté más completo.**

Este informe se centra en la creación de un repositorio de GitLab y la construcción de una imagen Docker.
Además, se incluirá un archivo de configuración de ejemplo para un balanceador/proxy reverso entre dos servidores Docker iguales.
Para lograr esto, se utilizarán 3 VMs en VirtualBox: Docker1, Docker2 y Nginx.
Al finalizar, se presentará el enlace al repositorio. En el mismo se podrá encontrar un readme con este mismo paso a paso.
Requisitos
1.  **Crear repositorio:**
Se creará un repositorio de GitLab, de nombre “dashboard”, donde se llevará adelante el proyecto;
2.  **Crear imagen Docker:**
Dentro de dicho repositorio se instalará una imagen de Docker;
3.  **Archivo de configuración para balanceador/proxy reverso:**
Se creará este archivo utilizando Nginx;


**Desarrollo**


**Creación de repositorio**


Se inicia sesión en GitLab y se procede a crear un nuevo proyecto, que tendrá las siguientes características y configuraciones:


Cabe aclarar que en GitLab también se deben agregar las llaves ssh de las VMs con Docker que vamos a utilizar.
Esto se hace mediante el comando:


>$ ssh-keygen -t rsa


Y obtenemos la llave pública revisando el fichero:
>$ cat .ssh/id_rsa.pub


**Preparación de entorno**


**Paso 1**


Primero, antes de generar las máquinas virtuales que utilizaremos, se crea una red NAT con la siguiente configuración:


**Nombre de la red:** VPC

**Prefijo IPv4:** 200.0.0.0/24


Y con la siguiente configuración de puertos:

http docker1 TCP 0.0.0.0 8080 200.0.0.50 8080

http docker2 TCP 0.0.0.0 8081 200.0.0.40 8081

http nginx  TCP 0.0.0.0 80 200.0.0.10 22

ssh docker1 TCP 0.0.0.0 5022 200.0.0.50 22

ssh docker2 TCP 0.0.0.0 4022 200.0.0.40 22

ssh ngnix TCP 0.0.0.0 1022 200.0.0.10 22
 
**Paso 2**


Una vez creada la red NAT, generamos una primera VM de nombre ‘docker1’, que tendrá la siguiente configuración de red:
>$ vim /etc/netplan/00-installer-config.yaml

```
network:
version: 2
renderer: networkd
ethernets:
enp0s3:
addresses:
200.0.0.50/24
addresses: [8.8.8.8, 8.8.4.4]
routes:
to: default
via: 200.0.0.2
```

 
Con estas configuraciones, podremos conectarnos a la VM por ssh utilizando este comando (en Windows):
> ssh -p 5022 istea@localhost


**Paso 3**


Comienza la instalación de Docker. Primero, se actualiza la lista de paquetes de software y se instala Docker:
>$ apt update && apt install docker docker.io


Luego, se añade el usuario ‘istea’ al grupo ‘docker’:
>$ gpasswd -a istea docker


**Paso 4**

Una vez instalado Docker, clonamos el repositorio de GitLab a nuestra instancia en el path ‘/home/istea’ con url ssh:
> $ cd /home/istea

> $ git clone git@gitlab.com:federico.pena/dashboard.git

> $ ls (para verificar que se clonó correctamente)

**Paso 5**

Se navega al directorio ‘/home/istea/dashboard’, que es donde estaremos trabajando:
> $ cd /home/istea/dashboard


Podemos revisar en qué branch estamos trabajando utilizando el comando:
> $ git branch

Una vez ahí, se configura el usuario que realizará las operaciones de Git:
> $ git config user.name "Federico P"

> $ git config user.email federico.pena@istea.com
 
**Paso 6**

Una vez configurado el repositorio en nuestra VM, descargamos el paquete de la aplicación Dashboard y el archivo de configuración de apache2:
> $ wget https://github.com/twbs/bootstrap/releases/download/v5.3.3/bootstrap-5.3.3-examples.zip

> $ wget https://gitlab.com/sergio.pernas1/album-jueves/-/raw/main/apache2.conf

También instalamos la aplicación unzip para poder descomprimir el archivo:

> $ apt install unzip
Una vez instalado unzip, descomprimimos el archivo:

> $ unzip bootstrap-5.3.3-examples.zip

**Paso 7**

Se crea un directorio ‘/html’ dentro de ‘/dashboard’, y se mueven los directorios necesarios para la app Dashboard:

> $ mv bootstrap-5.3.3-examples/assets html

> $ mv bootstrap-5.3.3-examples/dashboard html


Eliminamos el archivo .zip y el resto de los directorios que no vamos a utilizar:

> $ rm bootstrap-5.3.3-examples.zip -r

> $ rm -r bootstrap-5.3.3-examples*

Entonces, ‘/home/istea/dashboard’ queda con los siguientes directorios:
 
**Paso 8**

Dentro del directorio ‘html’, se edita el fichero ‘index.html’ para diferenciarlo de un futuro archivo, agregándole un “1” al título:

> $ vim html/index.html

**Paso 9**

Una vez que tenemos todos los archivos necesarios distribuidos de la manera correcta, realizamos las operaciones Git necesarias para que estos cambios impacten en el repositorio real:

> $ git add .

> $ git commit -m "dashboard-application"

> $ git push origin main

Y verificamos en GitLab que esté hecho el push correctamente:
 
**Configuración de Docker**

**Paso 1**

Como ya instalamos Docker en la etapa anterior, ahora procedemos a crear una imagen Dockerfile:

> $ vim Dockerfile

Y dentro de este fichero, colocamos lo siguiente:

```
FROM ubuntu
RUN apt update && apt install -y apache2
COPY html/ /var/www/html/
COPY apache2.conf /etc/apache2/apache2.conf
EXPOSE 80 443
CMD ["apache2ctl", "-D", "FOREGROUND"]
```


Una vez configurado este fichero, ejecutamos el siguiente comando, que va a ejecutar lo que colocamos dentro del fichero anterior para generar una imagen de Docker:

> $ docker build -t dashboard-image .

**Paso 2**

Una vez generada la imagen de Docker, se procede a lanzar el contenedor:

> $ docker run -d --name dashboard -p 8080:80 dashboard-image

Y verificamos que se haya lanzado correctamente:

> $ docker ps
 
Comprobamos que esté corriendo el contenedor en el puerto especificado:

> $ curl localhost:8080
 
**Paso 3**

Tras comprobar que funciona correctamente el contenedor, otra vez enviamos los cambios al repositorio:

> $ git add .

> $ git commit -m "dashboard-application"

> $ git push origin main

Y verificamos en GitLab que esté hecho el push correctamente:

**Paso 4**

Una vez que tenemos configurado correctamente docker1, clonamos esta máquina virtual en VirtualBox para obtener docker2. A la misma se le cambian las siguientes configuraciones:

- IP: 200.0.0.40
- Hostname: docker2

```
network:
version: 2
renderer: networkd
ethernets:
enp0s3:
addresses:
200.0.0.40/24
addresses: [8.8.8.8, 8.8.4.4]
routes:
to: default
via: 200.0.0.2
```

Con estas configuraciones, podremos conectarnos a la VM por ssh utilizando este comando (en Windows):

> ssh -p 4022 istea@localhost
 
**Paso 5**

Dentro de esta nueva VM, purgamos los rastros de la instancia de Docker anterior (ya que es clonada):

> $ docker system prune

Dentro del directorio ‘html’, se edita el fichero ‘index.html’ para diferenciarlo del archivo anterior, cambiando el “1” por un “2” en el título:

> $ vim html/index.html

**Paso 6**

Eliminamos la imagen Dockerfile que creamos y la volvemos a crear para que se apliquen estos cambios:

> $ docker image rm dashboard-image

> $ docker build -t dashboard-image .

Ejecutamos una instancia de Docker con el puerto correspondiente, que fue asignado al principio en la red NAT:

> $ docker run -d --name dashboard -p 8081:80 dashboard-image

Se comprueba nuevamente que el contenedor se esté ejecutando en el puerto especificado:

> $ curl localhost:8081
 
**Configuración de Nginx**

**Paso 1**

Creamos una tercera VM en la que instalaremos Nginx, que lo usaremos como proxy reverso y balanceador.
Comenzamos cambiando el nombre de host a ‘nginx’ y editando la configuración de red para que se asemeje a lo establecido anteriormente:

> $ hostnamectl set-hostname nginx

> $ reboot

> $ vim /etc/netplan/00-installer-config.yaml

```
network:
version: 2
renderer: networkd
ethernets:
enp0s3:
addresses:
200.0.0.10/24
addresses: [8.8.8.8, 8.8.4.4]
routes:
to: default
via: 200.0.0.2
```

Se cambia la configuración de la VM en VirtualBox para que se conecte a la red NAT creada anteriormente.
Con estas configuraciones, podremos conectarnos a la VM por ssh utilizando este comando (en Windows):

> ssh -p 1022 istea@localhost

**Paso 2**

En esta nueva VM, se instala Nginx, se activa y se verifica si está funcionando el servicio:

> $ apt install nginx

> $ systemctl enable nginx

> $ systemctl status nginx

**Paso 3**

Navegamos al directorio ‘/etc/nginx/sites-available’ y creamos el fichero ‘dashboard.net.conf’ con la siguiente configuración:

> $ cd /etc/nginx/sites-available

> $ vim dashboard.net.conf

```
upstream docker-backends {
server 200.0.0.50:8080;
server 200.0.0.40:8081;
}
server {
listen 80;
location / {
proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;
proxy_pass http://docker-backends/;
}
}
```

Navegamos al directorio ‘/etc/nginx/sites-enabled’ y eliminamos el fichero ‘default’:
>$ rm default

**Paso 4**

Una vez configurado el fichero, creamos un enlace simbólico de la configuración generada a ‘/etc/nginx/sites-enabled/’ para activar el virtual host y reinciamos el servicio para aplicar esta configuración:

> $ ln -s /etc/nginx/sites-available/dashboard.net.conf /etc/nginx/sites-enabled/

> $ systemctl reload nginx.service


**Enlace a repositorio de GitLab:**

https://gitlab.com/federico.pena/dashboard.git

**Fuentes**

https://docs.gitlab.com/ee/topics/git/index.html

https://docs.nginx.com/nginx/admin-guide/web-server/

https://pastebin.com/F32jCJ0g (config netplan)

